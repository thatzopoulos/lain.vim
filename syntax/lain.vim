if exists("b:current_syntax") 
  finish 
endif 


syn case match 

syn keyword     lainDirective         include
syn keyword     lainDeclaration       let


hi def link     lainDirective         Statement
hi def link     lainDeclaration       Type 


syn keyword     lainStatement         return let
syn keyword     lainConditional       if else

hi def link     lainStatement         Statement
hi def link     lainClass             Statement
hi def link     lainConditional       Conditional
hi def link     lainBranch            Conditional
hi def link     lainLabel             Label
hi def link     lainRepeat            Repeat


syn match       lainDeclaration       /\<fn\>/
syn match       lainDeclaration       /^fn\>/

syn match comment "#.*$"    contains=@Spell,lainCommentTodo
syn match comment "\/\/.*$" contains=@Spell,lainCommentTodo

syn keyword     lainCast              int str array


hi def link     lainCast              Type


syn keyword     lainBuiltins          len 
syn keyword     lainBuiltins          puts
syn keyword     lainBuiltins          puts
syn keyword     lainBuiltins          last
syn keyword     lainBuiltins          push
syn keyword     lainBuiltins          rest
syn keyword     lainBuiltins          first

syn keyword     lainBoolean           true false
syn keyword     lainNull              null

hi def link     lainBuiltins          Keyword 
hi def link     lainNull              Keyword
hi def link     lainBoolean           Boolean


" Comments; their contents 
syn keyword     lainTodo              contained TODO FIXME XXX BUG 
syn cluster     lainCommentGroup      contains=lainTodo 
syn region      lainComment           start="#" end="$" contains=@lainCommentGroup,@Spell,@lainTodo


hi def link     lainComment           Comment 
hi def link     lainTodo              Todo 


" lain escapes 
syn match       lainEscapeOctal       display contained "\\[0-7]\{3}" 
syn match       lainEscapeC           display contained +\\[abfnrtv\\'"]+ 
syn match       lainEscapeX           display contained "\\x\x\{2}" 
syn match       lainEscapeU           display contained "\\u\x\{4}" 
syn match       lainEscapeBigU        display contained "\\U\x\{8}" 
syn match       lainEscapeError       display contained +\\[^0-7xuUabfnrtv\\'"]+ 


hi def link     lainEscapeOctal       lainSpecialString 
hi def link     lainEscapeC           lainSpecialString 
hi def link     lainEscapeX           lainSpecialString 
hi def link     lainEscapeU           lainSpecialString 
hi def link     lainEscapeBigU        lainSpecialString 
hi def link     lainSpecialString     Special 
hi def link     lainEscapeError       Error 
hi def link     lainException		Exception

" Strings and their contents 
syn cluster     lainStringGroup       contains=lainEscapeOctal,lainEscapeC,lainEscapeX,lainEscapeU,lainEscapeBigU,lainEscapeError 
syn region      lainString            start=+"+ skip=+\\\\\|\\"+ end=+"+ contains=@lainStringGroup
syn region      lainRegExString       start=+/[^/*]+me=e-1 skip=+\\\\\|\\/+ end=+/\s*$+ end=+/\s*[;.,)\]}]+me=e-1 oneline
syn region      lainRawString         start=+`+ end=+`+ 


hi def link     lainString            String 
hi def link     lainRawString         String 
hi def link     lainRegExString       String

" Characters; their contents 
syn cluster     lainCharacterGroup    contains=lainEscapeOctal,lainEscapeC,lainEscapeX,lainEscapeU,lainEscapeBigU 
syn region      lainCharacter         start=+'+ skip=+\\\\\|\\'+ end=+'+ contains=@lainCharacterGroup 


hi def link     lainCharacter         Character 


" Regions 
syn region      lainBlock             start="{" end="}" transparent fold 
syn region      lainParen             start='(' end=')' transparent 


" Integers 
syn match       lainDecimalInt        "\<\d\+\([Ee]\d\+\)\?\>" 
syn match       lainHexadecimalInt    "\<0x\x\+\>" 
syn match       lainOctalInt          "\<0\o\+\>" 
syn match       lainOctalError        "\<0\o*[89]\d*\>" 


hi def link     lainDecimalInt        Integer
hi def link     lainHexadecimalInt    Integer
hi def link     lainOctalInt          Integer
hi def link     Integer                 Number

" Floating point 
syn match       lainFloat             "\<\d\+\.\d*\([Ee][-+]\d\+\)\?\>" 
syn match       lainFloat             "\<\.\d\+\([Ee][-+]\d\+\)\?\>" 
syn match       lainFloat             "\<\d\+[Ee][-+]\d\+\>" 


hi def link     lainFloat             Float 
"hi def link     lainImaginary         Number 


if exists("lain_fold")
    syn match	lainFunction	"\<fn\>"
    syn region	lainFunctionFold	start="\<fn\>.*[^};]$" end="^\z1}.*$" transparent fold keepend

    syn sync match lainSync	grouphere lainFunctionFold "\<fn\>"
    syn sync match lainSync	grouphere NONE "^}"

    setlocal foldmethod=syntax
    setlocal foldtext=getline(v:foldstart)
else
    syn keyword lainFunction	fn
    syn match	lainBraces	"[{}\[\]]"
    syn match	lainParens	"[()]"
endif

syn sync fromstart
syn sync maxlines=100

hi def link lainFunction		Function
hi def link lainBraces		Function

let b:current_syntax = "lain" 
