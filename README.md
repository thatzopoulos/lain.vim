# lain.vim

## Description
This is a Vim plugin that provides Lain file detection and syntax highlighting. It requires Vim 8 or higher for full functionality. Some things may not work in earlier versions.

## Installation
* [vim-plug][vp]:
  * Add `Plug 'https://gitlab.com/thatzopoulos/lain.vim.git'` to `~/.vimrc`
  * `:PlugInstall` or `$ vim +PlugInstall +qall`
* [Vim8 packages][vim8pack]:
  * `git clone https://gitlab.com/thatzopoulos/lain.vim ~/.vim/pack/plugins/start/lain.vim`
* [Vundle][v]:
  * Add `Plugin 'thatzopoulos/lain.vim'` to `~/.vimrc`
  * `:PluginInstall` or `$ vim +PluginInstall +qall`
  * *Note:* Vundle will not automatically detect lain files properly if `filetype
on` is executed before Vundle. Please check the [quickstart][vqs] for more
details. Errors such as `Not an editor command: lainFmt` may occur if Vundle
is misconfigured with this plugin.
* [Pathogen][p]:
  * `git clone --depth=1 https://gitlab.com/thatzopoulos/lain.vim.git ~/.vim/bundle/lain.vim`
* [dein.vim][d]:
  * Add `call dein#add('thatzopoulos/lain.vim')` to `~/.vimrc`
  * `:call dein#install()`
* [NeoBundle][nb]:
  * Add `NeoBundle 'thatzopoulos/lain.vim'` to `~/.vimrc`
  * Re-open vim or execute `:source ~/.vimrc`

## screenshot

![Vim](screenshot.png)
